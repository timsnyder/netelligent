$(function(){
	$(document).foundation();
	
	function place_gradient(){
		var offset = 280;
		var body = document.querySelector('body');
		var page = document.querySelector('#page');
		var new_pos = ((body.scrollWidth - page.scrollWidth) / 2) - offset;
		body.style.backgroundPositionX = new_pos + "px";	
	}
	
	place_gradient();
	
	window.addEventListener('resize', function(){
		place_gradient();	
	}, false);
	
	if(document.querySelector('#slideshow')){
		$('.flexslider').flexslider();
	}
	
});